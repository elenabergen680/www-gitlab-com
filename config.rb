require 'generators/direction'
require 'generators/releases'
require 'generators/org_chart'
require 'extensions/breadcrumbs'
require 'extensions/partial_build'
require 'lib/homepage'
require 'lib/mermaid'
require 'lib/plantuml'

###
# Page options, layouts, aliases and proxies
###

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '/blog/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_COMMIT_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/blog/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :external_pipeline,
         name: :frontend,
         command: "source/frontend/pipeline.sh #{build? || environment?(:test) ? '' : ' --watch'}",
         source: "tmp/frontend",
         latency: 3

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :partial_build

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  activate :minify_css
  activate :minify_javascript
  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Populate the direction and releases pages only on master or development machines.
  # That will help shave off some time of the build times on branches.
  if ENV['INCLUDE_GENERATORS'] == 'true' || !ENV.key?('CI_SERVER')
    # Direction page
    if ENV['PRIVATE_TOKEN']
      content = {}
      direction = Generators::Direction.new

      wishlist = direction.generate_wishlist # wishlist, shared by most pages
      direction_all_content = direction.generate_direction(Generators::Direction::STAGES) # /direction/
      direction_cicd_content = direction.generate_direction(Generators::Direction::CICD_STAGES) # /direction/cicd/
      direction_dev_content = direction.generate_direction(Generators::Direction::DEV_STAGES) # /direction/dev/
      direction_ops_content = direction.generate_direction(Generators::Direction::OPS_STAGES) # /direction/ops/
      direction_enablement_content = direction.generate_direction(Generators::Direction::ENABLEMENT_STAGES) # /direction/ops/

      Generators::Direction::STAGES.each do |name|
        # Fetch content for per-team pages
        skip = %w[release verify package] # these do not have a team page
        content[name] = direction.generate_direction(%W[#{name}]) unless skip.include? name # /direction/name/
      end

      stage_contribution_content = direction.generate_contribution_count(data.stages) # /direction/maturity/
      stage_velocity = direction.generate_stage_velocity(data.stages) # /direction/maturity/

      milestones = direction.generate_milestones # /releases/gitlab-com

      # Set up proxies using now-fetched content for shared pages
      proxy '/direction/index.html', '/direction/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/upcoming-releases/index.html', '/upcoming-releases/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/releases/gitlab-com/index.html', '/releases/gitlab-com/template.html', locals: { milestones: milestones }, ignore: true
      proxy '/direction/cicd/index.html', '/direction/cicd/template.html', locals: { direction: direction_cicd_content }, ignore: true
      proxy '/direction/dev/index.html', '/direction/dev/template.html', locals: { direction: direction_dev_content }, ignore: true
      proxy '/direction/ops/index.html', '/direction/ops/template.html', locals: { direction: direction_ops_content }, ignore: true
      proxy '/direction/enablement/index.html', '/direction/enablement/template.html', locals: { direction: direction_enablement_content }, ignore: true
      proxy '/direction/maturity/index.html', '/direction/maturity/template.html', locals: { stage_contributions: stage_contribution_content, stage_velocity: stage_velocity }, ignore: true
      proxy '/direction/kickoff/index.html', '/direction/kickoff/template.html', locals: { direction: direction_all_content }, ignore: true

      Generators::Direction::STAGES.each do |name|
        # And for team pages
        skip = %w[release verify package] # these do not have a team page
        proxy "/direction/#{name}/index.html", "/direction/#{name}/template.html", locals: { direction: content[name], wishlist: wishlist }, ignore: true unless skip.include? name
      end
    end

    ## Releases page
    releases = ReleaseList.new
    proxy '/releases/index.html', '/releases/template.html', locals: {
      list: releases.generate,
      count: releases.count
    }, ignore: true
  end
end

org_chart = OrgChart.new
proxy '/company/team/org-chart/index.html', '/company/team/org-chart/template.html', locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }
  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Analyst reports
data.analyst_reports.each do |report|
  next unless report.url

  proxy "/analysts/#{report.url}/index.html", '/analysts/template.html', locals: {
    report: report
  }, ignore: true
end

# Category pages for /product
data.categories.each do |key, category|
  next unless category.body && category.maturity && (category.maturity != "planned") && category.marketing

  proxy "/product/#{key.dup.tr('_', '-').downcase}/index.html", '/product/template.html', locals: {
    category: category,
    category_key: key
  }, ignore: true
end

# Event pages
data.events.each do |event|
  next unless event.url

  proxy "/events/#{event.url.tr(' ', '-')}/index.html", '/events/template.html', locals: {
    event: event
  }, ignore: true
end

# Hiring chart pages
Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
  proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# Conversion pages under /forms
data.forms.each do |form|
  proxy "/forms/#{form.url.tr(' ', '-')}/index.html", '/forms/template.html', locals: {
    form: form
  }, ignore: true
end

# Webcast pages
data.webcasts.each do |webcast|
  proxy "/webcast/#{webcast.url.tr(' ', '-')}/index.html", '/webcast/template.html', locals: {
    webcast: webcast
  }, ignore: true
end

# Reseller page
data.resellers.each do |reseller|
  proxy "/resellers/#{reseller.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').downcase.to_s.tr(' ', '-')}/index.html", '/resellers/template.html', locals: {
    reseller: reseller
  }, ignore: true
end

# Release Radars /webcast/monthly-release
data.release_radars.each do |release_radar|
  proxy "/webcast/monthly-release/#{release_radar.name.tr(' ', '-').downcase}/index.html", '/webcast/monthly-release/template.html', locals: {
    release_radar: release_radar
  }, ignore: true
end

# create new job listing instance when /jobs/apply reached
proxy "/jobs/apply/index.html", '/jobs/apply/template.html',
  locals: { listData: Gitlab::Homepage::Jobs::JobsListing.new.list_data }, ignore: true

# create routes to job page using job-listing template
Gitlab::Homepage::Jobs::JobsListing.new.jobs.each do |job|
  proxy "/jobs/apply/#{job[:title].downcase.strip.tr(' ', '-').gsub(/[^\w-]/, '')}-#{job[:id]}/index.html",
    "/jobs/apply/job-listing/template.html",
    locals: { job: job },
    ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Proxy case study pages
if @app.data.respond_to?(:case_studies)
  data.case_studies.each do |filename, study|
    proxy "/customers/#{filename}/index.html", '/templates/case_study.html', locals: { case_study: study }
  end
end

page '/404.html', directory_index: false

# Don't include the following into the sitemap
ignore '/direction/*' unless ENV['PRIVATE_TOKEN']
ignore '/frontend/*'
ignore '/templates/*'
ignore '/includes/*'
ignore '/upcoming-releases/template.html'
ignore '/releases/template.html'
ignore '/releases/gitlab-com/template.html'
ignore '/company/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore 'source/job-families/check_job_families.py'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'

# See https://gitlab.com/gitlab-com/infrastructure/issues/4036
proxy '/development/index.html', '/sales/index.html'
