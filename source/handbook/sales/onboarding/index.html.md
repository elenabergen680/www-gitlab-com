---
layout: markdown_page
title: "Sales Onboarding"
---
## **The Goals of Sales Onboarding**
Sales onboarding at GitLab is a blended learning experience (virtual, self-paced learning path paired with an immersive, hands-on in-person workshop called Sales Quick Start) focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first 30 days or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading discovery calls to begin building pipeline by or before the end of their first several weeks on the job. There are some formal learning components with a heavy emphasis on social learning (learning from others) and learning by doing to help bridge the knowing-going gap. Some of our learning goals pertaining to Sales Onboarding include:

*  Create relationships with colleagues and mentors and gain a strong understanding of GitLab culture
*  Develop a high-level understanding of the DevOps industry and GitLab competitors
*  Understand GitLab’s value framework (including, but not limited to, products, services, and GitLab values)
*  Understand personas and learn how to be audible ready with each of them on a discovery call
*  Gain a functional understanding of GitLab’s sales cycle
*  Learn how to utilize company resources (including, but not limited to, our handbook, the GitLab tool itself, Outreach, Chorus.ai, Clari, Zoom, Google Suite, and SFDC)
*  Gain a strong understanding of Command of the Message and MEDDPICC and become adept at trap-setting questions, discovery questions, and being audible-ready

## **Graduating from Sales Onboarding**
In order to officially “graduate” from Sales Onboarding at GitLab, we have identified a few components that are considered milestones of achievement (many of these must be done concurrently to ensure completion):

*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver GitLab Value Pitch
*  Articulate the Command of the Message Mantra
*  Complete a series of discovery calls:
   - Submit 1 recorded mock discovery call in SQS pre-work
   - Complete 1 mock discovery call at the SQS Workshop
   - Submit 1 “Live Lead” after the SQS Workshop within 30 days
*  Review and obtain approval from your manager for a territory and account plan
*  Close first deal within the first 90 days of starting at GitLab

## **Sales Onboarding Process**
1.  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
    - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
1.  In the "Day 1 Accounts and Paperwork" section of the general GitLab onboarding issue, sales managers are instructed to create an Access Request (sales role-based templates are available in the list of template on [this page](https://gitlab.com/gitlab-com/access-requests)
1.  In the "Sales Division" section of that issue, Sales Enablement is tagged with the action to add the new sales team member to the Sales Quick Start learning path in Google Classroom according to the SQS Workshop they plan to attend. This learning path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.
1.  The new sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quick Start learning path  
    - When possible, Sales Enablement will also update the new team member's onboarding issue and/or add a comment to explicitly reference the link to their Sales Quick Start learning path
    - In the "New Team Member" section, there is a specific action for the new hire to "Complete your Sales Quick Start learning path"
    - See what's included in the virtual, self-paced [Sales Quick Start learning path in Google Classroom](/handbook/sales/onboarding/#sales-quick-start-learning-path)
    - Non-Sales team members can choose to add themselves to the SQS Google Classroom Master Learning Path regardless of role if interested in understanding what new Sales Team Members are expected to complete prior to attending an SQS Workshop (Sales team members will be added to their appropriate cohort’s Google Classroom Learning Path by Sales Enablement)
        -  [SQS Google Classroom Master Learning Path](https://classroom.google.com/u/0/c/NDAzODA3NzEzMDla)
        -  Code: kdran7e

## **Targeted Sales Roles**
*  Targeted roles for the Sales Quick Start learning path include: 
   - Enterprise and Public Sector Strategic Account Leaders (SALs)
   - Mid-Market Account Executives (AEs)
   - SMB Customer Advocates (CAs)
   - Inside Sales Reps (ISRs)
*  Customer Success (e.g. Solution Architects, Technical Account Managers, and Professional Services Engineers) and Sales Development Rep (SDR) roles have their own separate onboarding process (but many of the same elements are shared)

## **Sales Quick Start Workshop**
*  After completing the virtual, self-paced Sales Quick Start learning path, new sales team members (along with new Solution Architects, Technical Account Managers, and Sales Development Rep) participate in an interactive in-person workshop where participants practice applying this new knowledge in fun and challenging ways including mock calls, role plays, and group activities
   - As a result, new sales team members exit Sales Quick Start more confident and capable in their ability to lead an effective discovery call with a customer or prospect
   - Participants also benefit by establishing a community of similarly-tenured peers and more experienced mentors and colleagues to support each others' growth and development at GitLab
*  See what's included in the [Sales Quick Start 3-day workshop](/handbook/sales/onboarding/#sales-quick-start-workshop)
*  **Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery)**
   - Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way
*  Future iteration of this process will define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## **Swag for New Sales Team Members**

As a part of Sales Onboarding, each new Americas Sales team member is allowed to order one Swag Marketing Kit through this form. As of right now, there is no Swag Marketing Kit available for our other regions, but the Marketing team is working towards developing one.

## **Sales Quick Start Learning Path**

### 1.Welcome to GitLab Sales! 
*  **Let's Git To Know Each Other! (Assignment, 5 minutes, 1 Point - Pre Work)**
   - Please complete this brief [google form](https://docs.google.com/forms/d/e/1FAIpQLScXH3QSAcqUP4mRJsqUWbn7BUJS_SYJVjFg2oXqOoOwzBMzLA/viewform) to introduce yourself.
*  **What is GitLab? (Material - Video, 3 Minutes)**
   - GitLab is a single application for the entire DevOps lifecycle. [Watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
*  **GitLab Culture (Material - Video, 3 Minutes)**
   - Every year, our entire remote workforce gets together in one location for the GitLab Summit. We use this time to bond, build community, and get a bit of work done. It's an essential part of the GitLab experience—watch the video to learn more about our culture, and what it's like to be on our globally distributed team.
   - [Watch the video](https://www.youtube.com/watch?v=Mkw1-Uc7V1k)
   - Read the [Handbook](https://about.gitlab.com/company/culture/gitlab-101/)
*  **Everyone Can Contribute (Material - Video, 3 Minutes)**
   - Learn more about how we live out our Contribute value! [Watch the video](https://www.youtube.com/watch?v=V2Z1h_2gLNU).
*  **Org Chart (Material - Handbook - 10 Minutes)**
   - You can see more information about team members and who reports to whom on the team page. Throughout the course you will be asked to schedule a few brief meetings with your peers. Keep in mind that it is always ok to put a meeting on someone's calendar, if they can't make it and decline it is not a problem. We hope you enjoy getting to know your super cool co-workers!
   - Check out the [org chart](https://about.gitlab.com/company/team/org-chart/) and the [Team Page](https://about.gitlab.com/company/team/)

### 2. DevOps Technology Landscape
*  **The Software Development Lifecycle (Material - Video - 9 Minutes)**
   - This video provides a nice overview of the SDLC. For added context, check out the GitLab page that covers each stage in the life cycle. 
   - [YouTube - Software Development Lifecycle in 9 Minutes! ](https://www.youtube.com/watch?v=i-QyW8D3ei0)
   - [Dev Ops Lifecycle - Handbook](https://about.gitlab.com/stages-devops-lifecycle/)
   - [YouTube - How is Software Made? ](https://www.youtube.com/watch?v=UuX-GnYWNwo)
*  **The Industry In Which GitLab Competes (Material - Video - 11 Minutes)**
   - Sid Sijbrandij, CEO of Gitlab, discusses the overall industry where Gitlab competes.
   - [You Tube - The Industry Gitlab Competes In](https://www.youtube.com/watch?v=qQ0CL3J08lI)
*  **Review & Subscribe: Industry Insights (Assignment - 1 Point - Pre Work)**
   - Review and subscribe to the following blogs recommended by Sid to get the latest industry insights.
   - [https://news.ycombinator.com/](https://news.ycombinator.com/)
   - [https://thenewstack.io/](https://thenewstack.io/)
   - [https://martinfowler.com/](https://martinfowler.com/)
   - [https://about.gitlab.com/blog/](https://about.gitlab.com/blog/)
*  **What is DevOps? (Material - Video - 25 Minutes )**
   - In this video, we compare the traditional DevOps daisy chain to GitLab's vision for a Complete DevOps solution. Even with the adoption of DevOps, serious challenges continue to exist. Developers and operators used to be separate groups with separate tools. Complete DevOps reimagines the scope of tooling to include both developers and operations teams in one unified solution.
   - [GitLab Blog ](https://about.gitlab.com/blog/)
   - [DevOps Terms ](https://enterprisersproject.com/article/2019/8/devops-terms-10-essential-concepts)
   - [Concurrent Dev Ops](https://drive.google.com/open?id=1kRFvypIHbtIQg8lQ4nBDGWm4GvPd9gPN&authuser=0)
   - [YouTube - Why Concurrent DevOps](https://www.youtube.com/watch?v=bDTYHGEIeM0)
*  **Deliver Better Products Faster (Material - Sales Collateral)**
   - In today’s modern landscape of rapidly changing priorities, rising expectations from customers,
and new market opportunities, IT leaders must discover new ways to streamline delivery of digital
applications and systems. The pace of change in the market has reached a point where the need for
velocity and rapid response is the new imperative. To remain competitive, organizations must reduce
cycle time.

   - [Reduce Cycle Time to Deliver Value](https://drive.google.com/open?id=12l8j6wY7qeiYMT6DYz0QRm95GGxxIedF&authuser=0)
*  **Traditional DevOps Toolchain vs. GitLab (Material - Video - 5 Minutes)**
   - Compare the traditional DevOps Daisy Chain of disparate tools to an integrated solution for Complete DevOps. GitLab delivers a seamlessly integrated platform for developers and operators to collaborate in real-time and move ideas into production faster.
   - [YouTube - Traditional DevOps Daisy Chain](https://www.youtube.com/watch?v=YHznYB275Mg)
   - [Manage Your Toolchain Before It manages You! ](https://drive.google.com/open?id=1tU4kG3sGIfb_zvECSJAr3sybPgXLs7g6&authuser=0)
   - [YouTube - You’re Living Like This? ](https://www.youtube.com/watch?v=w6X4Ha1oC6I)
*  **QUIZ: Increase Operational Efficiencies (Assignment - 25 Minutes - 6 Points - Pre Work)**
   - GitLab is a single application for the entire DevOps lifecycle! Please read the case study ["Manage Your Tool Chain Before it Manages You" ](https://drive.google.com/file/d/1tU4kG3sGIfb_zvECSJAr3sybPgXLs7g6/view?usp=sharing)by Forrester to answer the questions in the [Google Form Quiz.](https://forms.gle/namX7dcg1dbo5KQQ7)
*  **A Seismic Shift in Application Security (Material - Sales Collateral)**
   - How to integrate and automate security in the DevOps Lifecycle.
   - [A Seismic Shift In Application Security](https://drive.google.com/open?id=1RKWGGmms1WBSynWNIrAJRMIOZG_ainqg&authuser=0)
   - [2019 Global Developer Report: DevSecOps](https://drive.google.com/open?id=1anFlAQoyePAQYBxGvuUFwGbPcoZftE26&authuser=0)
*  **Question: Industry Coffee Chat (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - Please schedule a 15 minute coffee chat with your manager, a sales peer, or mentor about what they know now about the industry/market in which GitLab competes that they wish they knew earlier. This is a public forum for all of the students and teachers in the class! Please briefly summarize what you learned. Let's crowdsource some knowledge!

### 3. Our Customers
*  **Personas & Pain Points (Material - Video / Handbook - 20 Minutes)**
   - Introducing Personas and Pain Points. David Dulany, Sales Development Consultant, reviews the concepts of Personas and Pain Points and why they are important in setting the context for your messaging.
   - [You Tube - Introducing Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU)
   - [Handbook - Roles & Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)
*  **VP of App Dev (Material - Video - 10 Minutes)**
   - Watch Product Marketing Manager William Chia talk about the VP of App Dev persona.
   - [YouTube - VP App Dev](https://www.youtube.com/watch?v=58qDalA5o6Q)
*  **DevOps Director (Material - Video - 10 Minutes)**
   - [YouTube - Director of DevOps](https://www.youtube.com/watch?v=5_D4brnjwTg)
   - [DevOps Director Persona Presentation](https://drive.google.com/open?id=1C4wSY2g8vPjNdhOyRFH_bjtDGM0psZJViMfTACgIVCU&authuser=0)
*  **Head of IT (Material - Video - 10 Minutes)**
   - [YouTube - Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
*  **Chief Architect (Material - Video - 10 Minutes)**
   - [YouTube - Chief Architect](https://www.youtube.com/watch?v=qyELotxsQzY)
   - [Chief Architect Persona Presentation](https://drive.google.com/open?id=1HEelJip2Gnu0RDUVqfOwRYrxCtMS3Xf_qJBJ9Zr9ZTg&authuser=0)
*  **Question: Personas & Pain Points (Assignment - 1 Point - Pre Work)**
   - Please share your thoughts on the question below. This is a classroom discussion board, let's crowdsource some knowledge! Why is it important to understand different buyer personas as a salesperson?
*  **GitLab Digital Transformation CxO Discovery Guide (Material - 10 Minutes)**
   - [GitLab Discovery Guide](https://drive.google.com/open?id=1R6is7t4Ph3-p4tGJbDq0RhezT4j-3P0rluMV3H9-Rho&authuser=0)
*  **Marketing Materials Repository (Material - Gitlab.com - 10 Minutes)**
   - Check out the GitLab.com [marketing materials repository](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources/downloads) to see all of our best and most relevant sales collateral. Please be careful to only view or download these files, and feel free to send downloaded copies to your clients to help them understand GitLab value.
*  **Customer Success Stories & Proof Points (Material - Handbook - 10 Minutes)**
   - References are an age old tenet of sales pros! Your prospective clients will definitely be impressed by the positive business outcomes of our customers. Check out our [customer case studies](https://about.gitlab.com/customers/) and [proof points](https://drive.google.com/open?id=1gwLqlJKiiDxqsIaJ5H_NLsRgDD32cKO1-4wS9ivBijs&authuser=0) on GitLab value.

### 4. Our Portfolio
*  **GitLab Pitch Deck (Material - Video - 15 Minutes)**
   - Check out the latest corporate pitch deck that you can use to speak with your customers about GitLab. Watch the[ video](https://drive.google.com/file/d/1vRgU1o-o4kcOblQCxNi3h6xrN7KQZY1H/view) to hear John Jeremiah with [Product Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) deliver the GitLab [pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing)
*  **Question: Pitch Deck Video (Material - Video - 15 Minutes)**
   - Please write one thing you learned or found interesting from watching the GitLab Pitch Deck video.
*  **Feature Comparison (Material - Handbook - 10 Minutes)**
   - Check out the [feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/) chart to learn what's included in each package.
*  **Why Sell Ultimate/Gold? (Material - Handbook - 10 Minutes)**
   - Take a look at the [handbook link](https://about.gitlab.com/pricing/ultimate/) to understand what the best plans have to offer!
*  **Pricing (Material - Handbook - 10 Minutes)**
   - Check out the chart to understand our [pricing model](https://about.gitlab.com/pricing/). For additional context take a look at the [handbook page on pricing](https://about.gitlab.com/handbook/ceo/pricing/).
*  **GitLab Direction (Material - Handbook - 10 Minutes)**
   - Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle. We aim to make it faster and easier for groups of contributors to deliver value to their users, and we achieve this by enabling: Faster cycle time, driving an improved time to innovation, Easier workflows driving increased collaboration and productivity. Our solution plays well with others, works for teams of any size and composition and for any kind of project, and provides ongoing actionable feedback for continuous improvement. You can read more about the principles that guide our prioritization process in our [product direction handbook.](https://about.gitlab.com/direction/#single-application)
*  **Product Maturity (Material - Handbook - 10 Minutes)**
   - GitLab has a broad scope and vision, and we are constantly iterating on existing and new features. Some stages and features are more mature than others. To convey the state of our feature set and be transparent, we have developed a [maturity framework](https://about.gitlab.com/direction/maturity/) for categories, application types, and stages.
*  **Use Cases (Material - Handbook - 10 Minutes)**
   - A [customer use case](https://about.gitlab.com/handbook/use-cases/) is: A customer problem or initiative that needs a solution and attracts budget. Defined In customer terms: Often aligned to industry analyst market coverage (i.e. Gartner, Forrester, etc. write reports on the topic) These are discrete problems that we believe GitLab solves and are reasons customers choose GitLab (hence which we should seek out in prospects)

*  **Selling Professional Services (Material - Video - 30 Minutes)**
   - Our [Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/) team is made up of not only GitLab subject matter experts but seasoned DevOps professionals who have experience in deploying and maintaining both large-scale applications as well as creating and teaching best practices throughout the SDLC. Our experts help lead Concurrent DevOps Transformations, providing direct support to our customer’s strategic business initiatives. GitLab's Professional Services team exists to enable your clients realize the full value of their GitLab installation. We can provide direct implementation support to ensure the GitLab installation is resilient and secure. We also offer migration services to facilitate the transition to GitLab by delivering a clean dataset to resume operations at once. Our education and specialized training provide training in best practices, such as CI/CD, version control, metrics, and more.
   - You can also watch the [sales enablement session](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/sales-enablement/) about how to sell services.

### 5. Sales Process
*  **Overview: Command of the Message & MEDDPICC (Assignment - 1 Hour - 10 Points - Pre Work)**
   - Check out the [video](https://drive.google.com/open?id=1gvYLAP7kzbJ0dFIK2dG3WI3lI7VSnIWo&authuser=0) for an overview on Command of the Message, and MEDDPICC. The [PowerPoint](https://drive.google.com/open?id=1VrhUxk6wr6mTaMS1nbqP_WErC--W65dSi5YKm1K6sOM&authuser=0) in the video is attached as well as the Command of the Message [participant guide](https://drive.google.com/open?id=1rFTcWNxUKLQBTGyZKhs8pCMLzml5hAfn&authuser=0).
*  **Command of the Message Full E-Learning Course (Assignment - 2 Hours - 10 Points - Pre Work)**
   - Please sign in to Lyearn and complete the Command of the Message Fast Start eLearning before your in person training. **Please note: The welcome and enrollment emails will come from the following sender name and email address: “Command Center Admin” – fm-no-reply@lyearn.com. Please check your junk, spam and clutter folders if you did not receive these emails.
*  **MEDDPICC Full E-Learning Course (Assignment - 2 Hours - 10 Points - Pre Work)**
   - Please sign in to [Lyearn](https://gitlab.lyearn.com/#/login) and complete the MEDDPICC full eLearning course. **Please note: The welcome and enrollment emails will come from the following sender name and email address: “Command Center Admin” – fm-no-reply@lyearn.com. Please check your junk, spam and clutter folders if you did not receive these emails.
*  **GitLab Value Framework (Material - Sales Collateral - 35 Minutes)**
   - The GitLab [value framework](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ&authuser=0) is one of the most useful tools available for salespeople. Take a look to understand our value drivers, how to uncover customer needs, and how to articulate value and differentiation. A [framework summary](https://drive.google.com/open?id=1BawkSEbejPKx2EVgOqxtrhzHlCTGEFNzvoIF4hW3bT4&authuser=0) is also avaliable for quick reference. 
*  **Question - Command of the Message eLearning (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - Schedule a 15 minute coffee chat with your manager to ask questions around any areas of confusion about our value framework, value drivers, defensible differentiators, CXO digital transformation discovery guide, or Command of the Message or MEDDPICC in general. Please write a question you had, and what insights your manager provided.
*  **Social Selling 101 ( Material - Video - 20 Minutes)**
   - Social selling is the art of using social networks to find, connect with, and nurture your customers and prospects. Watch the [video](https://www.youtube.com/watch?v=w-C4jts-zUw) and use this [guide](https://drive.google.com/open?id=1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A&authuser=0) to learn how to make a profile that will resonate with your prospects.
*  **Question: Business Development (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - Business development is hard... because not everyone gets marketing qualified leads or has a big referral network. Take a look at the [Sales Development](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/) handbook page to understand more about your XDR partners and their processes. Please share 1-2 insights on strategy or best practices from your experience. How do you consistently keep the pipeline full of leads? Let's crowdsource some best practices!
*  **Question: Account Development (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - One of the most important parts of a salesperson's job is account development. Your clients bought from you once, and they are very likely to do it again! Please share 1-2 insights on strategy or best practices from your experience. How do you keep your clients buying more and sending referrals? Let's crowdsource some experience!


### 6. Sales Action
*  **Buyer Brief: Role Play Scenario (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - You've got the money and the power, you're the buyer! Create your own business, or use an account you're currently working on to role play as the buyer with a partner. Once completed, please share the [attached document](https://docs.google.com/document/d/1Zuy4z2YHZR0GXdQB_zexiknDKllgRab59wFWj3kpVnU/edit?usp=sharing) with your role play partner so they can prepare to be the seller in your role play.
*  **Role Play Preparation (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Please fill out the [attached document](https://docs.google.com/document/d/1jwLo3GYA81VNcXg7vHTRF7iMkF7YihV7a362yPtZx0o/edit?usp=sharing) to prepare for your mock call based on the information in the buyer brief provided by your partner. HINT: Use the [Value Framework](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ&authuser=0) to help fill in the blanks! 
*  **1st Mock Call (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Record your mock call with zoom, then upload the recording to Google Drive to submit the assignment.


### 7. Competitive Advantages & Strategy
*  **Competitor Overview (Material - Handbook - 30 Minutes)**
   - There are a lot of [DevOps tools](https://about.gitlab.com/devops-tools/) out there. As a single application for the entire DevOps life cycle, GitLab can remove the pain of having to choose, integrate, learn, and maintain the multitude of tools necessary for a successful DevOps tool chain. However, GitLab does not claim to contain all the functionality of all the tools listed here. Click on a DevOps tool to compare it to GitLab. Last thing dont forget to log into [Crayon](https://app.crayon.co/intel/gitlab/battlecards/), our competitive intelligence platform to view all of our latest competitive advantages.
*  **GitLab vs. GitHub (Material - Video - 3 Minutes)**
   - No... we are not the same company! This short YouTube video on [GitLab vs. GitHub](https://www.youtube.com/watch?v=s8DCpG1PeaU) covers some basic differences.
*  **Phone-A-Friend: Competitors (Assignment - 45 Minutes - 10 Points - Pre Work)**
   - Talk with 3 tenured GitLab sales professionals (individual contributors or people managers) and ask them for their tips and tricks on successfully beating the competition. Please take notes from your calls and briefly summarize one or two things you learned from each conversation in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdCwvGRS_-fV9J57KGYIjkXNEUHDigKoPXss-kf9r3Zjozo9w/viewform?usp=sf_link) to submit the assignment.
*  **Question: Discovery & the Competition (Assignment - 10 Minutes - 10 Points - Pre Work)**
   - A trap setting question is meant to trap the competition... not the customer. Take a look at our defensible differentiators in the Value Framework and write your own open ended trap setting question for a specific competitor based on one of our defensible differentiators. Lets crowdsource some good questions! 


### 8. Tools & Technology
*  **GitLab Tech Stack Details (Material - Handbook - 10 Minutes)**
   - Take a look to understand all the [tools that GitLab uses](https://about.gitlab.com/handbook/business-ops/tech-stack/) to keep the business running smoothly.
*  **Update Your LinkedIn Profile! (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Check out the [slide deck](https://drive.google.com/open?id=1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A&authuser=0) on creating a great profile that will look good to your prospects and clients! 
*  **DiscoverOrg 101 for Account Executives (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Watch the [GitLab Unfiltered YouTube video](https://youtu.be/4wLiyP2FcUM) session with Mid Market Account Executives and Sean Breen, Senior Manager, Learning & Development at DiscoverOrg. If you are unable to view the GitLab Unfiltered YouTube channel: Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. If you do not see an invitation in your Inbox, please check the Pending Invitations section of your GSuite account. Last thing... please add the [Discover.org chrome extension](https://chrome.google.com/webstore/detail/discoverorg-sales-intelli/jdanmmkadklcdnmmhodecjodjljkekja?hl=en-US) to your browser.
*  **You've Got Issues! (Assignment - 20 Minutes - 10 Points - Pre Work)**
   - Please find the [Sales Enablement Sandbox](https://gitlab.com/gitlab-com/sales-team/sales-enablement-sandbox) Project, and read the handbook page ["No Tissues with Issues"](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/101/). Create an issue with a label, due date, weight, and assign it to yourself. Submit the link to your issue to complete this assignment. Also take a look at the [Markdown syntax guide](https://www.markdownguide.org/basic-syntax/) to learn more about how to code using Markdown. This will be very helpful as you create issues and merge requests.
*  **Salesforce - Booking Orders (Material - Handbook - 10 Minutes)**
   - [Learn how to create](https://about.gitlab.com/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) Accounts, Contacts, Opportunities, and Quotes in Salesforce.

### 9. Sales Support
*  **Sales Handbook (Material - Handbook - 10 Minutes)**
   - This is definitely bookmark worthy material! The [sales handbook](https://about.gitlab.com/handbook/sales/) is your source of truth for everything sales!
*  **Solution Architects (Material - Handbook - 10 Minutes)**
   - [Solutions Architects (SA)](https://about.gitlab.com/handbook/customer-success/solutions-architects/) are the trusted advisors to GitLab prospects and customers during the presales motion, demonstrating how the GitLab application and GitLab Professional Services address common and unique business requirements. SA's add value to GitLab by providing subject matter expertise and industry experience throughout the sales process. SA's identify and propose comprehensive solutions to complex business problems and workflow scenarios after compiling inputs from customer conversations. Desired inputs include pain points, role-based visibility concerns, opportunities for improved efficiencies, current technologies and tools, corporate initiatives, target outcomes and more. SA's also act as the technical liaison between the sales team and other groups within GitLab, engaging GitLab employees from other teams as needed in order meet the needs of the customer.
*  **Customer Onboarding (Material - Handbook - 10 Minutes)**
   - [Technical Account Managers](https://about.gitlab.com/handbook/customer-success/tam/gemstones/) and Professional Services Engineers should work closely together throughout the on-boarding process, with support from Solutions Architects and Strategic Account Leaders/Account Managers where appropriate. Customer on-boarding is a 45 day time period. All implementation and training must be scheduled to be delivered within the first 30 days of purchase. The Technical Account Manager is responsible for ensuring that this happens.
*  **Support for GitLab Team Members (Material - Handbook - 10 Minutes)**
   - [GitLab Support](https://about.gitlab.com/handbook/support/internal-support/#what-does-the-support-team-do) provides technical support to our Self-managed and GitLab.com customers for the GitLab product. We are not internal IT Support, so we probably can't help you with your MacBook, 1Password or similar issues.
*  **GitLab Terms & Conditions (Material - Handbook - 10 Minutes)**
   - The following [terms and conditions](https://about.gitlab.com/terms/) govern all use of the GitLab.com website, or any other website owned and operated by GitLab which incorporate these terms and conditions) (the “Website”), including all content, services and support packages provided on via the Website. The Website is offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, procedures that may be published from time to time on this Website by GitLab (collectively, the “Agreement”).
*  **The GitLab Legal Team (Material - Handbook - 10 Minutes)**
   - You can reach out to the [Legal Team](https://about.gitlab.com/handbook/legal/) on the #legal Slack chat channel. The legal Slack chat channel is reserved for everyday legal questions. If you are making a request that requires some sort of deliverable, please do not use the legal Slack chat channel. Slack is reserved for immediate, informal communications. Also, please do not share confidential information on Slack that is not meant for the entire company to see, and do not use it to seek legal advice. You can email the legal team at legal@gitlab.com.
*  **Where to Find Sponsored Marketing Events (Material - Handbook - 10 Minutes)**
   - Take a look at the [marketing issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) to see when and where we will be hosting marketing events.


### In-Class Assignments 
*  **Searching GitLab Like a Pro (Assignment - 10 Minutes - 10 Points - In-Class)**
   - At GitLab, we're prolific at documenting what we do in the handbook, the website, and in GitLab documentation. This may make it difficult to find specific pieces of content. Google already indexes all our public facing pages and there is a search modifier google offers that will help. Take a look at the handbook page on how to [set up your search engine like the pros!](https://about.gitlab.com/handbook/tools-and-tips/searching/)
*  **Role Play Notes (Assignment - 10 Minutes - 10 Points - In-Class)**
   - Please submit your notes from one of the role plays that you observed using the [notes template](https://docs.google.com/document/d/1bzuO-ngACn4Z1S55Z7EtNOb2HYCZbcBZZRYIVpkeMtw/edit?usp=sharing). Dont forget to check out the [handbook page](https://about.gitlab.com/handbook/sales/#sales-note-taking) on how we take notes at GitLab. 
*  **Group Activity: Positioning Value, Resolving Objections & Closing (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://docs.google.com/forms/d/e/1FAIpQLSfwZiy__U1cl0kB3vOKl7GiQmcDgnBptT8G4qL_ZSw0gdC_hA/viewform?usp=sf_link), and please answer the following questions as a team. Be prepared to share your answers with the class. 
*  **Call Planning & Preparation - Group Activity (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://docs.google.com/forms/d/e/1FAIpQLSfwZiy__U1cl0kB3vOKl7GiQmcDgnBptT8G4qL_ZSw0gdC_hA/viewform?usp=sf_link), and please answer the following questions as a team. Be prepared to share your answers with the class


### Post-Class Assignments
*  **Outreach (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - Please complete the Outreach learning path. Time to complete is approximately 1 Hour. 
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **548fj4q** and click Join.
*  **Chorus.ai (Assignment - Classroom - 30 Minutes - 10 Points - Post Work)**
   - Please complete the Chorus.ai learning path. Chorus is a powerful conversation analytics platform that will help you expand your knowledge, ramp up faster, and adopt best practices! Time to complete is approximately 30 minutes.
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **me2cmyj** and click Join.
*  **Clari for Salespeople (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - The Clari assignment is only for Strategic Account Leaders, Mid-Market Account Executives, and SMB Customer Advocates. SAs, TAMs, SDRs, and ISRs may disregard and do not need to complete the course. Clari is a sales forecasting technology that helps salespeople drive more pipeline, accelerate revenue, and forecast accurately. Time to complete is approximately 1 hour.
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **e7hfgg0** and click Join.
*  **MEDDPICC Opportunity Qualification (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - Use this [template](https://docs.google.com/document/d/17Rr03SHKQ2XXlOnvuhz68F8pTxNhr6nOK70TBwB63oc/edit?usp=sharing) to begin qualifying an account suggested by your manager.
*  **Custom Pitch Deck (Assignment - 1 Hour - 10 Points)**
   - Create a custom Pitch Deck for an account that you're currently prospecting.
*  **Custom Pitch Deck Recording (Assignment - 1 Hour - 10 Points)**
   - Record yourself delivering your custom Pitch Deck with zoom to practice.
*  **Live Lead (Assignment - 1 Hour - 10 Points)**
   -Please submit the link to your live lead using Chorus. If your lead was not recorded with Chorus, please upload the Zoom recording to Drive and submit the Drive file.
 

### 10. Course Evaluation & Feedback
*  Every participant is asked to please let us know how we can do better by taking [this brief survey](https://docs.google.com/forms/d/e/1FAIpQLSch3PLSzmoPUCSyhHUVwUfFSSPwiVGl2lAhRIc_bCmqZ6us6g/viewform?usp=sf_link)


## **Sales Quickstart Workshop Agenda**
*  **Day 1** 
      - Welcome / Introductions / SQS Objectives
      - Breakout: Call Prep & Planning
      - Mock Discovery Call Exercise #1
      - Lunch
      - GitLab.com Basics Lab 1
         - Inspired by [No Tissues for Issues](https://bit.ly/GitLab-101) content
      - Mock Discovery Call Exercise #2
      - Breakout: Positioning Value, Resolving Objections & Closing
      - Day 1 Recap
      - Group Dinner
*  **Day 2**
      - Day 2 Outlook
      - Mock Discovery Call Exercise #3
      - Discussion: Product Tiering
      - Breakout: Territory / Account Planning
      - Lunch
      - Breakout: Territory / Account Planning (continued)
      - Mock Discovery Call Exercise #4
      - GitLab.com Basics Lab 2: Submit Your Own MR
      - GitLab Handbook Scavenger Hunt
      - Day 2 Recap
*  **Day 3**
      - Day 3 Outlook
      - Discussion: GitLab Alliances
      - Discussion: [GitLab Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
      - Discussion: Customer Stories
      - Discussion: Competitors
      - Lunch with Q&A
      - Feedback & Wrap-up
