---
layout: handbook-page-toc
title: "Using PathFactory"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Using PathFactory

PathFactory is our [content library](/handbook/marketing/corporate-marketing/content/#content-library). Digital marketing program managers work with marketing program managers to organize and curate assets into tracks that are then disseminated for use in Marketo, `about.gitlab.com`, and other campaign-related channels. If you require a track for a campaign or marketing initiative, please use the [PathFactory request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/pathfactory_request.md) in the [Digital Marketing Programs repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs). If you have another PathFactory request unrelated to building a target track, please create an issue in the [Digital Marketing Programs repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs) and assign `@sdaily`.


### Rules of engagement

| Team | DRI | Responsibilities |
| ------ | ------ | ------ |
| Digital Marketing Programs | `@sdaily` | Upload gated content, Web promoter strategy, QA, dissemination, and management of links, Removing expired assets, PathFactory analytics |
| Digital Marketing Programs | `@mnguyen4` `@shanerice` | Testing landing page (form fill > PF > experience) |
| Marketing Programs | MPM | Creation of target tracks related to campaigns, PathFactory analytics, Adding content to relevant tracks in intended order, Creation of recommended tracks, Form strategy for each track, Managing adding UTMs when activating in channels, Measuring success of workflow |
| Marketing Operations | `@nlarue` | QA of PathFactory scoring, QA of PathFactory scoring > MQL scoring, QA of data flow to SFDC |
| Corporate Marketing | Author | Upload content (blog posts, videos, etc.) |
| Field Marketing | FMM | Utilize links for follow-up emails |
| Product Marketing | PMM | Utilize links |

### PathFactory requests

1.  To make a small change to an existing content track (e.g. adding an asset, adjust current form strategy, adjust asset order, etc.), simply ping `@sdaily` on the issue you are working from - no need to create a separate issue for small changes.

### PathFactory training

*  [Knowledge base](https://lookbookhq.force.com/nook/s/kb)
*  [Getting started video series](http://successwith.pathfactory.com/c/lookbookhq-tutorial-?x=Blrk3E)

### Changelog

Periodically, significant changes are made or added to PathFactory and processes that affect overall data and reporting. As such we have a [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit?usp=sharing). Everyone with author access that is making additions or changes to assets, content tracks, or otherwise should add their changes to the changelog with a linked issue. Scenarios for adding to the changelog:

1.  Creation or launch of a new content track and where it will be used
1.  Creation or launch of a new website promoter
1.  Additions or changes to a form strategy within a live content track
1.  Additions or changes to assets within a live content track
1.  Expiration of an asset from the content library
1.  Change of a custom URL slug for an asset or content track and why
1.  Change of promote settings within a live content track and why

### Managing PathFactory

#### Uploading assets to the content library

**Before uploading content:**

1. Use the search to determine if it’s already been added.
1. Make sure you have the most valuable version of the asset (blog post vs. case study or PDF).
1. Make sure you have the most recent version of the asset.

**Uploading content:**

1. Follow the [blog style guide](/handbook/marketing/corporate-marketing/content/editorial-team/).
1. Copy/paste the title of the asset to the public and internal title.
1. Remove `| GitLab` from the public and internal title. Public and internal title should always match for the sake of consistency and search within the platform.
    1. **Public Title:** the title of the asset that you would be comfortable with prospects or customers seeing and makes sense to those outside of GitLab.
    1. **Internal title:** where you can apply your chosen naming conventions, and have the asset names include markers and abbreviations which are meaningful to the internal team.
1. Provide a clear and concise description of the content if one does not exist (most times you can simply grab the subheading from the page).
1. For content type, follow the [content tag map below](#tracking-content). 
1. Leave the engagement score at a default of 20 seconds with a score of 1.
1. Ensure language is set to English.
1. Leave Business Units and External ID blank.
1. **For video assets of any kind:** Use the prefix `[Watch]` before the title.
1. Update the custom URL slug to be descriptive of the content with no stop words (and, the, etc.).
    1.  **Please Note:** DO NOT change a custom URL slug that is part of a content track, this action can affect any links to this item that have been previously shared and break the asset consumption tracking via [listening campaign](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns). 
    1. If a URL slug needs to be modified, please open an issue, assign to `@sdaily` and cc `@jjcordz`.  
1. We force `https://` to content tracks by default. As a result, all assets must use `https://` in the link to work in the content track properly. If you upload content that is *not secure*, it *will not* show a lock icon next to the URL. Please manually add `https://` if it is not already there.
1. Do not use smart topics or extracted text when adding topics.  

**Bulk uploading new assets to the content library:**

You can bulk upload multiple assets to the content library via a CSV file.

1.  Make a copy of this [CSV template](https://drive.google.com/a/gitlab.com/file/d/1Ad4iSwHt2bdRKp5GGUZtrtzY5YShZlyI/view?usp=sharing) for your upload.
2.  Provide all the information in the template for your new assets and save as a CSV.
3.  Create an issue and tag `@sdaily` to review the template and upload to PathFactory.

#### Analyst reports

1. Expiry dates are not functional so you must manually deactivate the content if it’s past the expiry date.

### How content track promoters work

1. You can only use the `Header` feature with the `Sign Posts` and `Bottom Bar` promoters.
1. The `Header` is used to add additional branding to your content track.
1. The `Flow`, `Sign Posts`, and `Bottom Bar` cannot be used together. Choose 1 of the 3.
    1. **Flow:** Scrollable content menu allows visitors to jump ahead in their Content Track, or simply use the Next buttons to move forward.
    1. **Sign Posts:** Customizable Next and Previous buttons allow visitors to navigate through content. Used for more of a linear journey through the content.
    1. **Bottom Bar:** Collapsible content menu along page bottom.
1. The `End Promoter`, `Exit`, and `Inactivity` promoters can be used in conjunction with either the `Flow`, `Sign Posts`, or `Bottom Bar` promoters.
    1. **End Promoter:** Opens final asset in a new tab. 
        1. Available overrides:
            1. Link
            1. CTA Label
            1. Delay (seconds)
    1. **Exit:** Suggested content window appears when visitor tries to navigate away from the Content Track. 
        1. Available overrides:
            1. Headline
            1. Message
            1. Items to show (choose from assets within the current track)
            1. Delay (seconds)
    1. **Inactivity:** Message flashes on tab when left inactive.
        1. Available overrides:
            1. Inactive tab title
            1. Delay (seconds)

### Content track management

1. Ensure that the content track settings have the Meta Tag set to `No Index`
1. Every track should include a main CTA in the sidebar if using the `Flow` promoter.
1. **When a track is LIVE (being used):** change the target track title in use to `[LIVE] Name of track`.
1. **When testing a track:**
  1. Remove any extraneous `?` (there should only be one immediately after the end of the URL).
  1. Watch for extra `&` when appending UTMs.
  1. Ping `@sdaily` to test and ensure the experience is working as intended.
1. **When adding a PathFactory URL to Marketo:**
  1. Remove any extraneous `?` (there should only be one immediately after the end of the URL).
  1. Watch for extra `&`.
  1. Ping `@sdaily` to review the link before implementation for quality assurance purposes.
1. Before you edit, add, or remove an asset from a track, please create an issue and assign `@sdaily` to ensure that any live links on `about.gitlab.com` are updated accordingly.
1. If a piece of content is moving out of a track, please create an issue so links inside of nurture emails can be updated accordingly. Removing or changing an asset in a `[LIVE]` track can disrupt the user experience for the visitor and activate the `#all` track instead of the intended content track.
1. To ensure proper tracking of an asset in PathFactory, it should be included within a content track and not shared with an individual link.
1. Turn on the `Cookie Consent` before providing the approved content track link for live use.
1. Each content track has to have a unique name. You cannot use duplicate names for content tracks.

### Content track best practices

**Target Track**

1. Curated content
1. Known audience
1. Personalized journey (email, website, targeted display)
1. 5-7 pieces of content
1. Use target in webinar reg and follow-up
1. Use target to GUIDE

**Recommended tracks**

1. Dynamic content sequence (it will automatically move content to the top of the track that is performing well)
1. Anonymous audience
1. Personalized journey (web, general display, social)
1. Tracks the most popular journey (which pieces are being viewed, can be exported into a target track)
1. Use recommend to DISCOVER

**Explore page use cases**

1.  Resource center
1.  Event or webinar follow-up
1.  Co-branded resource page
1.  Personalized information hub

### Form strategy

Form strategy is used on content tracks to collect data from unknown visitors and should only be used when a track entry point is **not** from a webform or landing page (i.e. direct link from digital ad or web promoter). Not all content tracks will or should have form strategy turned on. The forms used in PathFactory are directly tied to currently existing Marketo forms. If the form strategy is implemented, please ensure `Show to Known Visitors` is left **unchecked**.  
* For help with PathFactory forms and workflows, please create an issue in the Marketing Operations repository and apply `MKtgOps` & `PathFactory` labels.   
* For help with adding form strategy to a content track, create an issue in the Digital Marketing Programs repository and ping `@sdaily`.

**Please Note:** We have [listening campaigns](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns) in Marketo set up to capture consumption of content that would have been gated had PathFactory not been implemented. The listeners also incorporate PathFactory activity into the [MQL scoring model](/handbook/business-ops/resources/#mql-scoring-model). This means that you do not need to add form strategy to a content track if entry point is from a landing page and there are listening campaigns set up for assets in your track that would normally be gated. [Please create an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues) in the Marketing Operations repo and assign `@jjcordz` to set up a lisetning campaign for an asset.

#### Adding form strategy to a content track

1.  Turn on the form strategy for the content track by clicking `View Form Strategy` located below the `Add Content` button.
2.  Determine whether the form strategy will be applied to individual assets or the entire track. For individual assets, you'll choose `Content Rules`; for form strategy on the entire content track, you'll choose `Track Rule`. You can have both turned on, but it is not recommended.

**Form strategy for individual assets:**

1.  Click `Add Rule` in the `Content Rules` row. 
2.  Select the `General Form (2074) LIVE` form.
3.  Under `Display Behavior`, click the dropdown and choose the assets where you want the form to show. (**Please Note:** only assets that you have added to the content track will show in the dropdown. If you want the form to show on an asset that *is not* in the track, you will need to add it first.)
4.  Select the amount of seconds you want to delay before the form shows on the asset. Ten seconds is the default selection.
5.  Select additional options for the form behavior. If you will be using the content track or individual asset links in an email, you are working with a known audience and therefore should only select `Show to unknown users`. This prevents forms being shown to users who are already known in Marketo. However, if you are using the form on the web or other channels, you'll want to select `Show to unknown users` only.
6.  Leave `If submitted, allow form to show again` turned off.
7.  You can `allow visitors to dismiss the forms` if it is not crucial to its use to have them submit their info. This decision ultimately lies with the directly responsible MPM.
8.  The option `Keep promoters active when form is shown` is also up to the directly responsible MPM. For example, if the `Flow` promoter is used on a content track, they will still be able to see the sidebar of avialable content while the form is shown to them. If this option is turned off, the visitor *will not* be able to click on any content in the sidebar until they fill out the form.

**Form strategy for content tracks:**

1.  Click `Add Rule` in the `Track Rule` row. 
2.  Select the `General Form (2074) LIVE` form.
3.  Under `Display Behavior` you can choose to serve the form based on the number of content assets viewed or the total time spent on the track. This decision lies with the directly responsible MPM.
4.  All other options for content track rules are the same for individual assets (see above).

### Using PathFactory links

**Important: All content tracks should be set up with custom URL slugs to prevent any future changes to the track from breaking the link and thus the user experience. If you change the custom URL slug after a PathFactory link has been implemeneted, those links have to be updated wherever they were used (ads, emails, website, etc.).**

1.  Only content track links are meant to be used and shared. Do not share individual asset links from the content library.
1.  PathFactory content tracks are meant to encourage content binging (visitors reading more than 1 asset to accelerate them in the pipeline by helping them self educate faster in one visit). So as best practice, have more than 1 asset in a track.
1.  You can use a content track link for multiple use cases as long as you apply UTMs appropriately. Applying UTMs helps us differentiate how the track performed across different channels.

#### Target track links

1.  Use the `Get Share URL` feature next to the title of the track. `Share links` are to be used in locations such as the website whereas `Email tracking links` are only for use in email (**Please note:** If it’s in email, it’s a known audience so don’t gate any assets in the track. Only use `share links` on the web and those tracks *can have* gated assets within PathFactory).
1.  If you want a particular asset to show first, that asset should be located in the first position of the target track. 
1.  If the link breaks or an asset is deleted, the user will be redirected from your content track to the `#all` track, which includes all assets uploaded to PathFactory. In a case where the user is not redirected to the `#all` track, they will be redirected to the `Fallback URL` which is set to `about.gitlab.com`.

#### Recommended track links

1.  To use a recommended track link, simply click on the first asset of the track and copy the link from the asset window on the right.

#### Appending UTMs to PathFactory links

1. First check and see if there is a question mark `?` already existing in the PF link. Typically there is one. The only time it won't have a `?` is when you set a custom URL.
1. If there is a question mark `?`, first add an ampersand `&` at the end of the PF link, followed by the UTMs. 
    1. For example:
        1. PF Link: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb`
        1. PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb&utm_source=email&utm_campaign=test`
1. If there is no question mark `?`, first add a question mark `?` at the end of the PF link, followed by the UTMs.
    1. PF Link: `https://learn.gitlab.com/c/10-strategies-to-red`
    1. PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?utm_source=email&utm_campaign=test`
1. For a PF Marketo link, it will typically already include a question mark "?". To add UTMs, first add an ampersand `&` at the end of the PF link, followed by the UTMs.
    1. Example: `https://learn.gitlab.com/c/devops-explained-git?x=GVFK3F&lb_email={{lead.Email Address}}&utm_source=email&utm_campaign=test`

### Live target tracks

If you want to make changes to a content track, please create an issue and tag `@sdaily`.

### Tracking content

Unsure what content topics align with your asset? Use the table below as a guideline to tag content you upload to PathFactory accordingly.

| Topic | Use | Example |
| ------ | ------ | ------ | 
| AWS | Content that relates Amazon Web Services. Likely use cases are case studies where the customer uses GitLab + AWS and integration information & tutorials. | [How to set up multi-account AWS SAM deployments with GitLab CI/CD](/blog/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/) |
| Agile delivery | Content that relates to the agile delivery process decision framework which emphasizes incremental and iterative planning. | [What is an Agile mindset?](/blog/2019/06/13/agile-mindset/) |
| Agile software development | Content that relates to the agile software development methodology which emphasizes cross-functional collaboration, continual improvement, and early delivery | [How to use GitLab for Agile software development](/blog/2018/03/05/gitlab-for-agile-software-development/) |
| Application modernization | Content that relates to the process of converting, refactoring, re-writing, or porting legacy systems to more modern programming and infrastructure. Content on this topic may cover cost/benefit of updating legacy systems, process, system, and culture changes, and toolstack comparisons. | [3 Strategies for implementing a microservices architecture](/blog/2019/06/17/strategies-microservices-architecture/) |
| Automation | Content that relates to using technology to automate tasks. Likely use cases are how automation impacts productivity and workflows, feature highlights & tutorials, and case studies. | [How IT automation impacts developer productivity](/blog/2019/05/30/it-automation-developer-productivity/) |
| Azure | Content that talks specifically about Microsoft Azure. Likely uses cases are tutorials on using GitLab + Azure cloud or competitive content. | [Competitive analysis page for Azure DevOps](/devops-tools/azure-devops-vs-gitlab.html)
| CI/CD | Content that covers continuous integration, continuous delivery, and continuous deployment. This content is likely to more technical, explaining tools, methods for implementation, tutorials, and technical use cases. | [A quick guide to CI/CD pipelines](/blog/2019/07/12/guide-to-ci-cd-pipelines/) |
| Cloud computing | Content that relates to the practice of using a network of remote servers hosted on the Internet to store, manage, and process data. Likely uses cases are content discussing various cloud models (public, private, hybrid, and multicloud), integrations, and tutorials. Some customer case studies may be tagged with this label if the case study is *primarily* about GitLab enabling their cloud computing model. | [Top 5 cloud trends of 2018: What has happened and what’s next](/blog/2018/08/02/top-five-cloud-trends/) |
| Cloud native | Content that relates container-based environments. Specifically, technologies are used to develop applications built with services packaged in containers, deployed as microservices and managed on elastic infrastructure through agile DevOps processes and continuous delivery workflows. | [A Cloud Native Transformation](/webcast/cloud-native-transformation/) |
| Containers | Content that relates to using, running, maintaining, and building for containers. | [Running Containerized Applications on Modern Serverless Platforms](https://www.youtube.com/watch?v=S8R7sSePAXQ)
| DevOps | Content that relates to DevOps methods, process, culture, and tooling. [Keys to DevOps success with Gene Kim](https://www.youtube.com/watch?v=dbkj0qXQ22A)
| DevSecOps| Content that relates specifically to integrating and automating security into the software development lifecycle. Content that relates to cybersecurity should be tagged `security` and not `devsecops`.| [A seismic shift in application security](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf) |
| Digital transformation |  | 
| GKE | Content that is specifically about Google Kubernetes engine and Google Cloud Platform. Likely use cases are integrations, tutorials, and case studies | [Demo: Deploy to GKE from GitLab](https://www.youtube.com/watch?v=u3jFf3tTtMk)
| Git | Content that relates to implementing and using the distributed version contronl system, Git. | [Moving to Git](/resources/downloads/gitlab-moving-to-git-whitepaper.pdf) |
| Jenkins | Content that is specifically about Jenkins. Likely uses cases are integrations, competitive, comparisons, and case studies. | [3 Teams left Jenkins: Here's why](/blog/2019/07/23/three-teams-left-jenkins-heres-why/) |
| Kubernetes| Contnet that relates to implementing and using kubernetes. Likely use cases are cost/benefits, tutorials, and use cases. | [Kubernetes and the future of cloud native: We chat with Kelsey Hightower](/blog/2019/05/13/kubernetes-chat-with-kelsey-hightower/) |
| Microservices |  |
| Multicloud |  |
| Open source |  |
| SCM |  |
| Security | Content that relates to cybersecurity and application security practices. | [When technology outpaces security compliance](/blog/2019/06/10/when-technology-outpaces-security-compliance/)
| Single application |  |
| Software development |  |
| Toolchain | Content that relates to toolchain and stack management. | [How to manage your toolchain before it manages you](/resources/downloads/201906-gitlab-forrester-toolchain.pdf)
| VSM | Content that relates to the topic of value stream mapping and management. Topics that fall under this tag may include cycle time, cycle analytics, and software delivery strategies and metrics. | [The Forrester Value Stream Management Report](/analysts/forrester-vsm/index.html) |
| Workflow | Content that relates to understanding and implementing workflows throughout the software development lifecycle. Likely uses are content that explains a particular workflow or how to set up a workflow in GitLab. For example: how a workflow might change when a level of automation is introduced. | [Planning for success](/resources/downloads/gitlab-planning-for-success-whitepaper.pdf) |

## PathFactory analytics

To request an analytics report on a content track, create an issue in the Digital Marketing programs repo and tag `@sdaily`.