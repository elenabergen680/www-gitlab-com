---
layout: markdown_page
title: "Section Direction - Ops"
---

- TOC
{:toc}
![Ops Overview](/images/direction/ops/ops-overview.png)

## Ops Section Overview

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/UnXIidZA9Vc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

The Ops Section comprises the [Configure](/direction/configure) and [Monitor](/direction/monitor) [stages of the DevOps lifecycle](/stages-devops-lifecycle/).
[Market analysts (internal - i)](https://drive.google.com/file/d/1B2KCSj_PF8ZhZqx_8Q6zFU2vldMu94lT/view) often describe these stages as IT automation and configuration management (ITACM, analogous to Configure) and IT operations management (ITOM, partially analogous to Monitor). 

The [total addressable market (TAM) for DevOps tools targeting these stages (i)](https://drive.google.com/file/d/1B2KCSj_PF8ZhZqx_8Q6zFU2vldMu94lT/view) is $2.2B today and expected to grow to $5.3B in 2023 (18.8% CAGR). 
This is a deep value pool and represents a significant portion of GitLab's expanding addressable market.
The [market is well established (i)](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) with players such as Splunk, New Relic and Datadog dominating market share in the Monitor stage and IBM (+RedHat), Hashicorp, Puppet and Chef splitting a large chunk of the fragmented market in the Configure stage. 

With so many existing strong players, GitLab's market share in these stages is effectively zero.
Our customers do utilize [Configure](https://app.periscopedata.com/app/gitlab/462967/Configure-Metrics) and Monitor capabilities today.
Primarily they:
* Use AutoDevOps and occasionally Serverless for quickly getting started with non-production, no-ops projects
* Occasionally use Auto DevOps for production workloads.
* Use Auto DevOps as a starting point to design modern devops workflows.
* Utilize GitLab's core features in Create and Verify to manage Infrastructure as Code and Serverless projects ([internal example](https://gitlab.com/gitlab-com/dev-resources))
* Value our integration with Kubernetes and Prometheus 
* Are interested in bringing Incident Management processes into GitLab to encourage collaboration within their team     
* Are interested in bringing monitoring of Serverless into GitLab

Our current R&D investment in Configure and Monitor stages is [40 team members](/company/team/?department=ops-section) (on a [plan to grow 205% Y/Y in 2019](/handbook/hiring/charts/ops-section/)).
The [Configure stage is considered in Year 3 maturity](/direction/maturity/#configure) and the [Monitor stage is considered in Year 1 maturity](/direction/maturity/#monitor).

## Feedback
We would appreciate your feedback on this direction page. Please take [our survey](https://forms.gle/ex9zZ4sKmE1FKiHr9), [email Kenny Johnston](mailto:kenny@gitlab.com) or [propose an MR](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/source/direction/ops/template.html.md.erb) to this page!

## Challenges
Despite significant growth in our R&D investment in 2019, we are constrained relative to our competition in our investment amount in the Ops section. This constraint coupled with our lack of brand identification with Enterprise Ops section buyers means we need to be very strategic in building our product capabilities. 

While the market for the Configure stage has many strong incumbent players, it is splintered across many traditional IT management tools (Ansible, Chef, Puppet). There are [emerging trends (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view) showing a consolidation on [containers (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p9) and [specifically Kubernetes (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p9) for cloud-native application development. This will have the effect of stymying commercial
 players from locking in market share based on proprietary tools. So while there are strong players, the current market dynamics means there is [upside](#opportunities) for us.

The market for the Monitor stage is aligned around key players [New Relic, Splunk, ServiceNow and Datadog](http://videos.cowen.com/detail/videos/recent/video/6040056480001?autoStart=true). Datadog, New Relic and Splunk are all headed towards integrated observability platforms based on logging and metrics. [ServiceNow (i)](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) is considered a standard for help-desk and Incident Management workflows. Strong existing players with large sustained R&D investment will make it difficult for GitLab to compete head-to-head for Monitor stage market share. 

Beyond the key players, there is also rapid innovation in new technologies making for a market where there is both proliferation of new technology, and consolidation of the winning technologies into comprehensive platform players. Ease of deployment for operational tools has aided this expansion. Developers, and operations teams can easily instrument and utilize new technologies alongside their applications with less risk, and in quicker time frames than under previous models where it required dilligent planning and months of installation and configuration. 

Competing tools are marketed as stand-alone point solutions while GitLab's Ops section capabilities will require customers to be using other stages of GitLab. This creates a narrow funnel for adoption with potential users coming only from the segment of the market already using, or willing to use, other stages of GitLab rather than broad-based market fit. This smaller adoption funnel will also affect the rate of learning. 

Few companies are able to successfull bridge between Developer and Operator personas. Building tools that satisfy both personas is difficult. Without deeply understanding new personas, market entrants end up with a checklist of modules that doesn't represent a cohesive and improved experience for users.

Outside of market challenges we have some internal ones as well. In general, we are not [dogfooding](/handbook/values/#dogfooding) the Ops section features sufficiently. This has the effect of [slowing our rate of learning](/handbook/product/#dogfood-everything), and putting us in the position of not having immediate real world validation of our product initiatives.

## Opportunities
As noted above, given strong existing players, and our relatively low R&D investment, product maturity and brand awareness compared to the competition, GitLab must be targeted in our investment. Here are some key opportunities we must take advantage of:

* **Increasing Developer buying power:** In the move to DevOps, [tools focused on enabling developers will garner increased share of wallet](http://videos.cowen.com/detail/videos/recent/video/6040056480001?autoStart=true). [71% of organizations (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p4) will achieve full DevOps adoption on some projects within a year. [35% of DevOps implementations include Monitor stage capabilities, 30% include Configure stage ones (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p14). 
* **IT operations skills gap:** DevOps transformations are hampered by a [lack of IT operations skills (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p19). Most organizations have taken to creating [infrastructure platform teams](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successful-engineering-8a9b6a4d2c8) to [minimize the amount of operations expertise required by DevOps team members (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view).  
* **Clear winner in Kubernetes:** Driven by software-defined infrastructure, cost management, and resiliency, organizations [are flocking to cloud-native application architectures (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view). Kubernetes is the clear winner in container orchestration platforms. [Gartner recommends (i)](https://drive.google.com/file/d/19MZsKTc8tvAAFQEWuxDGoTW1f_kCZ6GI/view) organizations choose vendors that support open-source software projects in the cloud native ecosystem ([CNCF](https://www.cncf.io/)). 
* **IaC and Continuous Configuration Management are precursors to AIOps:** AIOps strives to provide self-learning, self-healing IT operations massively impacting IT operations professionals and their tools. However, it is [early in the hype cycle (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view) and being successful will depend on DevOps teams first defining their infrastructure, operations and observability as code.

## 3 Year Strategy
In three years the Ops Section market will:
* Consolidate around Kubernetes as the defacto abstraction layer which application delivery teams interact with to deploy and maintain software
* Be clearly segmented between infrastructure platform delivery professionals, and DevOps application delivery professionals
* Have adapted DevOps processes to also include infrastructure and observability into CI/CD pipelines providing more responsive, more secure and higher quality production applications

As a result, in three years, Gitlab will:
* Be considered a critical tool for infrastructure platform delivery teams
* Enable strong collaboration between application and platform teams
* Provide easy adoption of IaC deployment patterns so DevOps teams have more insight and responsiveness to their application's production capabilities
* Provide a best-in-class monitoring solution which competes with modern APM solutions like Datadog

## Themes
Our direction for Ops is to enable today's modern best-practices in operations without the burden of specialized teams, multiple tools and heavy workflows that are the largest barriers to adoption in these stages of the DevOps lifecycle.
Our goal is to empower DevOps teams to own their code in production and have the tools to contribute not just to application feature development but to the characteristics of their applications as experienced by their end-users.

<%= partial("direction/ops/themes/1-ops-as-code") %>

<%= partial("direction/ops/themes/2-smart-feedback-loop") %>

<%= partial("direction/ops/themes/3-ops-for-all") %>

<%= partial("direction/ops/themes/4-no-ops") %>

## Useful Resources
Below are some additional resources to learn more about Ops:
* [The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations](https://www.amazon.com/dp/1942788002/)

## One Year Plans

### Ops Section Plan

In the next 12 months the Ops Section **must** focus on:
* **Dogfooding** - To immediately increase our rate of learning
* **Logging** - In order to build out a complete tool for operational tasks we will improve our logging capabilities based on common open-source standards
* **Dashboarding** - Visualization is critical to both the [instrumentation](https://gitlab.com/groups/gitlab-org/-/epics/1945) and [triage](https://gitlab.com/groups/gitlab-org/-/epics/1947) workflows in application operations. Rich, single application dashboarding will be critical to the success of GitLab's Ops features.
* **Incident Management** - In order to complete the feedback loop for our customers and provide a complete DevOps view to leaders, we will meet our users where they are in their DevOps journey
* **IaC** - In order to [skate where the puck is headed](/handbook/product/#why-cycle-time-is-important) we will improve the experience of using GitLab to source control your infrastructure and observability configuration
* **Serverless** - To get a stand on this new market, and support our customers' serverless needs

In doing so, over the next 12 months we will choose **NOT** to:
* Invest heavily in ease of use and experience for individuals attaching Kubernetes clusters to projects - While there are use cases for project-level attachment, it is a higher priority to provide Infrastructure Platform teams with an enhanced cloud-native management experience.
* Invest heavily in enhanced out-of-the-box configuration of monitoring including auto-anomoly detection and synthetic monitoring - We need to focus first on dogfooding and the nitty gritty of the three pillars of observability before moving to more advanced monitoring capabilities.
* Invest in app instrumentation across a number of legacy programming languages - We will pursue a modern first approach, meaning we will not focus on easy instrumentation of apps written in legacy programming languages.

### Stage Plans

<%= partial("direction/ops/plans/configure") %>

<%= partial("direction/ops/plans/monitor") %>

### Acquisition Plan

As a key part of our investment in expanding the breadth of GitLab, the stages and categories in the Ops section have a [higher appetite for acquisition](/handbook/acquisitions/#acquisition-target-profile). Over the next year, GitLab is interested in acquisitions that fill gaps in our current set of new and minimal categories in the [Configure](/direction/maturity/#configure) and [Monitor](https://about.gitlab.com/direction/maturity/#monitor) stages. There are also some other un-named categories which fulfill our [themes](#themes) above that we would consider acquisitions in. Those include:

Top priorities:
1. APM - Tools that immediately increase our capabilities in tracing and real user monitoring are a top priority. GitLab is immediately focused on improving the out of the box experience for instrumenting and setting up triage workflows of users applications.
1. Smart Feedback Loop - Tools that enable the continuous improvement of applications via efficient feedback loops from production to the Plan stage, including product telemetry, user forums, suggestion mechanisms and real-user monitoring. This is our second priority because it is a critical part of the DevOps process and GitLab has limited to no capabilities or planned categories today.

Additional options:
* Operations as Code - Tools and software that improve the ability to define observability systems and repeatedly deploy them alongside application code.
* Operations for All - Tools that help developers quickly understand complex infrastructure setups, including service catalogs, cluster cost optimization and observability interfaces 
* NoOps - New low-code, no-ops languages and application paradigms including no-backend languages such as [dark-lang](https://darklang.com/).


## Stages and Categories

The Ops section is composed of two stages, each of which contains several categories. Each stage has an overall strategy statement below, aligned to the themes for Ops, and each category within each stage has a dedicated vision page (plus optional documentation, marketing pages, and other materials linked below.)

<%= partial("direction/ops/strategies/configure") %>

<%= partial("direction/ops/strategies/monitor") %>

## What's Next

It's important to call out that the below plan can change any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization) as the product team at large. Issues will tend to flow from having no milestone, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.

<%= direction["all"]["all"] %>
