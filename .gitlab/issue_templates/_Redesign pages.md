
## This template is for requesting redesign or major updates of existing pages.

This repository only represents the marketing website and company handbook. For product related requests please see the following documentation [https://about.gitlab.com/handbook/product/](https://about.gitlab.com/handbook/product/)

# Every issue should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

### It is recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/website/team-documentation/#issue-labels)

### Let's begin

Below are suggestions. Please delete irrelevant sections. Any extra information you provide is beneficial.

#### What is/are the expected URL(s)

Does this match existing strategies? Has keyword research been done?

#### Briefly describe the page/flow

(Example: Add a new page comparing us to a specific competitor)

#### If applicable, do you have any relevant wireframes, layouts, content/data?

(Example: Google doc/sheet, balsamiq, etc)

#### What is the primary purpose of this page/flow?

(Example: Increase EE trials)

#### What are the KPI (Key Performance Indicators)

(Example: Premium signups per month as identified by Periscope dashboard URL)

#### What other purposes does this page/flow serve, if any?

(Example: Capture emails for campaign funnels)

#### What information is this page/flow trying to convey?

(Example: Forrester says GitLab is great)

### Who is the primary audience? Be specific.

(Example: Potential customers vs developers vs CTO vs Jenkins users)

#### Any other audiences?

(Example: Media outlets, investors, )

#### Where do you expect traffic to come from?

(Example: Homepage direct link, on-site blog post, social media campaign, targeted ads)

#### If applicable, are there any items which can be hidden at first glance but still need to be present?

(Example: Tooltips, collapsible sections, filterable items, different view-type toggles, view-more buttons, extra data)

#### Please provide some example websites for design guidance.

(Example: "Pinterest implements recommendations well", "Stripe has good payment UX", "I like the aesthetics of nike.com", animation examples, microinteractions, etc)

#### Will this change require any new or updated events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Will this change require any new or updated reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Will this change require any new or updated automation?

(Example: Create a test ensuring a job listing is never empty)

### Will this change require any page redirects?

(Example: redirect tld.com/url/asdf to tld.com/url/zxcv)

<!-- do not edit below this line-->

/label ~"mktg-website" ~"template::web-redesign"

cc @gl-website
