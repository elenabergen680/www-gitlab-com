
## This template is for requesting minor updates of existing pages.

This repository only represents the marketing website and company handbook. For product related requests please see the following documentation [https://about.gitlab.com/handbook/product/](https://about.gitlab.com/handbook/product/)

# Every issue should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

### It is recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/website/team-documentation/#issue-labels)

### Let's begin

Below are suggestions. Please delete irrelevant sections. Any extra information you provide is beneficial.

### What is/are the relevant URL(s)

(Example: https://about.gitlab.com/compare/asdf/)

### Briefly describe the page/flow

(Example: This page compares us to a specific competitor)

### What is the primary reason for updating this page?

(Example: Increase EE trials)

### What exactly do you want to change on this page?

(Examples: Add speakers to an event, update hyperlinks, increase button size. Please be specific with details.)

### What are the KPI (Key Performance Indicators)

(Example: Premium signups per month as identified by Periscope dashboard URL)

#### Will this change require any new or updated events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Will this change require any new or updated reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Will this change require any new or updated automation?

(Example: Create a test ensuring a job listing is never empty)

### Will this change require any page redirects?

(Example: redirect tld.com/url/asdf to tld.com/url/zxcv)

<!-- do not edit below this line-->

/label ~"mktg-website" ~"template::web-update"

cc @gl-website

